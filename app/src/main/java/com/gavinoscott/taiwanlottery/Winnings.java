package com.gavinoscott.taiwanlottery;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A result of a check for winnings
 */
public final class Winnings {
    public final String ticket;
    public final String date;
    public final Winning_Type type;

    public Winnings(String ticket, String date, Winning_Type type) {
        this.ticket = ticket;
        this.date = date;
        this.type = type;
    }
}
