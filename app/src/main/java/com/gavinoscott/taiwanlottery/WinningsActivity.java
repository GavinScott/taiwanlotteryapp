package com.gavinoscott.taiwanlottery;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class WinningsActivity extends AppCompatActivity {
    public static final String TEXT_KEY = "text";
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winnings);
        text = (TextView)findViewById(R.id.winnings_text_view);
        text.setText(getIntent().getExtras().getString(TEXT_KEY));
    }
}
