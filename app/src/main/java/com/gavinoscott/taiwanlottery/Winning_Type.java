package com.gavinoscott.taiwanlottery;

import java.util.HashMap;

/**
 * Type of winning
 */
public enum Winning_Type {
    SPECIAL (10000000),
    GRAND   (2000000),
    REGULAR (200000),
    MATCH_7 (40000),
    MATCH_6 (10000),
    MATCH_5 (4000),
    MATCH_4 (1000),
    MATCH_3 (200),
    OTHER (200);

    public final int prize;
    Winning_Type(int prize) {
        this.prize = prize;
    }

    public static HashMap<Integer, Winning_Type> getTypes() {
        HashMap<Integer, Winning_Type> map = new HashMap<>();
        map.put(7, MATCH_7);
        map.put(6, MATCH_6);
        map.put(5, MATCH_5);
        map.put(4, MATCH_4);
        map.put(3, MATCH_3);
        return map;
    }
}
