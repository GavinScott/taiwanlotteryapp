package com.gavinoscott.taiwanlottery;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class EnterWinnersActivity extends AppCompatActivity {

    EditText special;
    EditText grand;
    EditText reg1;
    EditText reg2;
    EditText reg3;
    EditText other;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_winners);
        special = (EditText)findViewById(R.id.special_input);
        grand = (EditText)findViewById(R.id.grand_input);
        reg1 = (EditText)findViewById(R.id.reg1_input);
        reg2 = (EditText)findViewById(R.id.reg2_input);
        reg3 = (EditText)findViewById(R.id.reg3_input);
        other = (EditText)findViewById(R.id.other_input);
    }

    public void onSubmitButton(View v) {
        ArrayList<Winnings> winnings = CalculateWinnings.calculateForEachTicket(this,
                special.getText().toString().trim(), grand.getText().toString().trim(),
                reg1.getText().toString().trim(), reg2.getText().toString().trim(),
                reg3.getText().toString().trim(), other.getText().toString().trim());

        if (winnings == null || winnings.size() == 0) {
            CustomLogger.Log("No winnings this time", "");
            special.setText("");
            grand.setText("");
            reg1.setText("");
            reg2.setText("");
            reg3.setText("");
            other.setText("");
            Toast toast = Toast.makeText(this, R.string.no_win_msg, Toast.LENGTH_LONG);
            TextView toastView = (TextView) toast.getView().findViewById(android.R.id.message);
            if( toastView != null) toastView.setGravity(Gravity.CENTER);
            toast.show();
        } else {
            CustomLogger.Log("Start winnings activity", "(successful win)");
            Intent intent = new Intent(this, WinningsActivity.class);
            intent.putExtra(WinningsActivity.TEXT_KEY,
                    CalculateWinnings.buildWinningsString(winnings));
            startActivity(intent);
        }
    }
}
