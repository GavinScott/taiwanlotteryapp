package com.gavinoscott.taiwanlottery;

import android.util.Log;

/**
 * A Logger that can be turned on and off
 */
public final class CustomLogger {
    private static final boolean SHOW_LOGS = false;

    public static void LogError(String tag, String msg) {
        if (SHOW_LOGS) {
            Log.e(tag, msg);
        }
    }

    public static void Log(String tag, String msg) {
        if (SHOW_LOGS) {
            Log.d(tag, msg);
        }
    }
}
