package com.gavinoscott.taiwanlottery;

import android.app.Activity;
import android.util.Log;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;

/**
 * Calculates winnings by searching for numbers
 */
public final class CalculateWinnings {

    public static ArrayList<Winnings> calculateForEachTicket(Activity act, String special,
                                                             String grand, String reg1, String reg2,
                                                             String reg3, String other) {
        // setup
        ArrayList<Winnings> winnings = new ArrayList<>();
        HashMap<String, String> entries = FileIO.getEntries(act);
        if (entries == null) {
            CustomLogger.LogError("ERROR", "Could not read entries");
            return winnings;
        }
        String[] regs = new String[]{reg1, reg2, reg3};

        // get maximum winnings for each entered ticket
        for (String ticket : entries.keySet()) {
            Winnings wins = getTicketWinnings(ticket, entries.get(ticket), special, grand,
                    regs, other);
            if (wins != null) {
                winnings.add(wins);
            }
        }

        // sort by prize amount
        Collections.sort(winnings, new Comparator<Winnings>() {
            @Override
            public int compare(Winnings o1, Winnings o2) {
                return Integer.valueOf(o2.type.prize).compareTo(o1.type.prize);
            }
        });

        return winnings;
    }

    public static String buildWinningsString(ArrayList<Winnings> winnings) {
        String result = "";
        CustomLogger.Log("Winnings length for str", Integer.valueOf(winnings.size()).toString());
        for (Winnings win : winnings) {
            CustomLogger.Log("WINNINGS", win.ticket);
            result += "#" + win.ticket + "\n";
            result += win.date + "\n";
            result += "NT$" + NumberFormat.getNumberInstance(Locale.US).format(win.type.prize) + "\n";
            result += "\n\n";
        }
        return result;
    }

    private static Winnings getTicketWinnings(String ticket, String date,
                                              String special, String grand, String[] regs,
                                              String other) {
        // special prize
        if (ticket.equals(special)) {
            CustomLogger.Log("MATCH SPECIAL", ticket);
            return new Winnings(ticket, date, Winning_Type.SPECIAL);
        }

        // grand prize
        if (ticket.equals(grand)) {
            CustomLogger.Log("MATCH GRAND", ticket);
            return new Winnings(ticket, date, Winning_Type.GRAND);
        }

        // regular prizes
        for (String reg : regs) {
            if (reg.length() == ticket.length()) {
                if (ticket.equals(reg)) {
                    CustomLogger.Log("MATCH REGULAR", ticket);
                    return new Winnings(ticket, date, Winning_Type.REGULAR);
                } else {
                    HashMap<Integer, Winning_Type> matchTypes = Winning_Type.getTypes();
                    for (int i = 7; i >= 3; i--) {
                        String shortReg = reg.substring(reg.length() - i);
                        String shortTicket = ticket.substring(ticket.length() - i);
                        if (shortTicket.equals(shortReg)) {
                            CustomLogger.Log("MATCH SUB-REG (" + i + ")", ticket);
                            return new Winnings(ticket, date, matchTypes.get(i));
                        }
                    }
                }
            }
        }

        // other prize
        if (other.length() == 3) {
            String shortTicket = ticket.substring(ticket.length() - other.length());
            if (shortTicket.equals(other)) {
                CustomLogger.Log("MATCH OTHER", ticket);
                return new Winnings(ticket, date, Winning_Type.OTHER);
            }
        }

        // no match
        return null;
    }
}
