package com.gavinoscott.taiwanlottery;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText)findViewById(R.id.text_input);
    }

    public void onSaveButton(View v) {
        String input = editText.getText().toString().trim();
        if (isValidInput(input)) {
            Toast toast;
            if (!FileIO.saveTicket(this, input)) {
                toast = Toast.makeText(this, R.string.save_bad_msg, Toast.LENGTH_LONG);
            } else {
                toast = Toast.makeText(this, R.string.save_good_msg, Toast.LENGTH_LONG);
                editText.setText("");
            }
            TextView toastView = (TextView) toast.getView().findViewById(android.R.id.message);
            if( toastView != null) toastView.setGravity(Gravity.CENTER);
            toast.show();
        }
    }

    public void onSearchButton(View v) {
        Intent intent = new Intent(this, EnterWinnersActivity.class);
        startActivity(intent);
    }

    public void onClearButton(View v) {
        DialogInterface.OnClickListener listener = new ClearClickListener(this,
                getResources().getString(R.string.clear_msg),
                getResources().getString(R.string.save_bad_msg));

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_question).setPositiveButton(R.string.yes_str,
                listener).setNegativeButton(R.string.no_str, listener).show();
    }

    private boolean isValidInput(String input) {
        if (input.length() != 8) {
            Toast toast = Toast.makeText(this, R.string.bad_input_msg, Toast.LENGTH_LONG);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            if( v != null) v.setGravity(Gravity.CENTER);
            toast.show();
            return false;
        }
        return true;
    }

    private class ClearClickListener implements DialogInterface.OnClickListener {
        private final Activity act;
        private final String toastMsg;
        private final String errMsg;

        public ClearClickListener(Activity act, String toastMsg, String errMsg) {
            this.act = act;
            this.toastMsg = toastMsg;
            this.errMsg = errMsg;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    CustomLogger.Log("Clearing!", "old data cleared");
                    Toast toast;
                    if (FileIO.clear(act)) {
                        toast = Toast.makeText(act, toastMsg, Toast.LENGTH_LONG);
                    } else {
                        toast = Toast.makeText(act, errMsg, Toast.LENGTH_LONG);
                    }
                    TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
                    if( v != null) v.setGravity(Gravity.CENTER);
                    toast.show();
                    break;
            }
        }
    }
}
