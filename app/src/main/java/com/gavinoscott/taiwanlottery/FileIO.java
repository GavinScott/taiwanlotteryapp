package com.gavinoscott.taiwanlottery;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

/**
 * Handles the file IO for ticket saving/searching
 */
public final class FileIO {
    public static final String FNAME = "LotteryNumbers.txt";

    /**
     * Saves a ticket to the local cache
     * @param number the ticket number
     * @return If saved successfully
     */
    public static boolean saveTicket(Activity act, String number) {
        CustomLogger.Log("Saving", "*" + number + "*");
        try {
            FileOutputStream fos;
            try {
                fos = act.openFileOutput(FNAME, Context.MODE_APPEND);
            } catch (FileNotFoundException ex) {
                fos = act.openFileOutput(FNAME, Context.MODE_PRIVATE);
            }
            String s = number + "|" + getCurrentDate() + "\n";
            fos.write(s.getBytes());
            fos.close();
        } catch (Exception e) {
            CustomLogger.LogError("Write Error", "Generic exception");
            return false;
        }
        return true;
    }

    /**
     * Gets a map of all entries to their dates
     */
    public static HashMap<String, String> getEntries(Activity act) {
        try {
            FileInputStream fis = act.openFileInput(FNAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            HashMap<String, String> entries = new HashMap<>();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                CustomLogger.Log("Found Number", "*" + line + "*");
                String[] parts = line.split("\\|");
                CustomLogger.Log("*" + parts[0] + "*", "*" + parts[1] + "*");
                entries.put(parts[0].trim(), parts[1].trim());
            }
            return entries;
        } catch (Exception e) {
            CustomLogger.LogError("Read Error", "Generic exception");
        }
        return null;
    }

    /**
     * Clears the ticket cache
     */
    public static boolean clear(Activity act) {
        ArrayList<String> validLines = new ArrayList<>();

        // get valid months
        int curMonth = getMonth(getCurrentDate());
        ArrayList<Integer> validMonths = new ArrayList<>(Arrays.asList(curMonth, curMonth - 1, curMonth - 2));
        if (curMonth == 1) {
            validMonths = new ArrayList<>(Arrays.asList(11, 12, 1));
        } else if (curMonth == 2) {
            validMonths = new ArrayList<>(Arrays.asList(12, 1, 2));
        }

        try {
            FileInputStream fis = act.openFileInput(FNAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                CustomLogger.Log("Found Number", "*" + line + "*");
                String[] parts = line.split("\\|");
                CustomLogger.Log("*" + parts[0] + "*", "*" + parts[1] + "*");
                if (validMonths.contains(getMonth(parts[1]))) {
                    validLines.add(line.trim() + "\n");
                    CustomLogger.Log("SAVING TICKET", line);
                } else {
                    CustomLogger.Log("DELETING TICKET", line);
                }
            }
        } catch (FileNotFoundException ex) {
            CustomLogger.Log("Read Error", "OK file not made yet");
            return true;
        } catch (Exception e) {
            CustomLogger.LogError("Read Error", "Exception in clear method");
            e.printStackTrace();
            return false;
        }

        // delete file
        act.deleteFile(FNAME);

        // rewrite
        try {
            FileOutputStream fos;
            try {
                fos = act.openFileOutput(FNAME, Context.MODE_PRIVATE);
            } catch (FileNotFoundException ex) {
                CustomLogger.LogError("Write Error", "Cant open file");
                return false;
            }
            for (String line : validLines) {
                fos.write(line.getBytes());
            }
            fos.close();
        } catch (Exception e) {
            CustomLogger.LogError("Write Error", "Generic exception");
            return false;
        }
        return true;
    }

    private static int getMonth(String date) {
        CustomLogger.Log("Finding month", date + " = " + date.substring(3, 5));
        return Integer.parseInt(date.substring(3, 5));
    }

    private static String getCurrentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(new Date());
    }
}
